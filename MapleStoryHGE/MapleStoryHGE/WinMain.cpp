
#include <iostream>
#include <Windows.h>
#include "HGE\include\hge.h"

HGE* pHge = NULL;

bool Update() {

	return false;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	pHge = hgeCreate(HGE_VERSION);

	if (pHge != NULL) {
		pHge->System_SetState(HGE_FRAMEFUNC, Update);//设置框架函数  
														//pHge->System_SetState(HGE_RENDERFUNC, RenderFunc);  
		pHge->System_SetState(HGE_TITLE, "第一个HGE窗体");//设置标题  
		pHge->System_SetState(HGE_SCREENWIDTH, 400);//设置窗口大小  
		pHge->System_SetState(HGE_SCREENHEIGHT, 300);
		pHge->System_SetState(HGE_WINDOWED, true);//设置是窗口显示还是全屏，要全屏的话不能设置大小  
		pHge->System_SetState(HGE_USESOUND, false);//是否使用声音  
												   //pHge->System_SetState(HGE_SHOWSPLASH, false);//让程序不要显示开始那段动画  

		if (pHge->System_Initiate()) {
			pHge->System_Start();
		}

		pHge->System_Shutdown();
		pHge = NULL;
	}
	delete pHge;

	return 0;
}

